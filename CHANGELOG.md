## Editoria

### 1.1.4

* Fix for unsaved changes warning in Wax.

### 1.1.3

* New design for the book builder, the dashboard and the theme.
* Use new version of the Wax Editor. The editor can now accept configuration options, layouts and has been broken down into three separate modules (pubsweet integration, react integration and core) for better separation of concerns.
* Renamed 'remove' button to 'delete' for consistency with the bookbuilder.
* Fixed issue with fragments disappearing when uploading multiple files.
* Renamed 'Front Matter' and 'Back Matter' to 'Frontmatter' and 'Backmatter'.
* Double clicking on a book in the dashboard will take you to the book builder for that book, instead of opening the renaming interface.
* The position of 'Edit' and 'Rename' actions in the dashboard have been swapped ('Edit' now comes first).
* Books in the dashboard now appear in alphabetical order.
* Diacritics work within notes in Wax
