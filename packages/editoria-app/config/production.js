const universal = require('./universal')

module.exports = {
  authsome: universal.authsome,
  bookBuilder: universal.bookBuilder,
  dashboard: universal.dashboard,
  pubsweet: universal.pubsweet,
  'pubsweet-client': universal.pubsweetClient,
  'pubsweet-component-ink-backend': universal.inkBackend,
  'pubsweet-server': universal.pubsweetServer,
  validations: universal.validations
}
